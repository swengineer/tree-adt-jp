/**
 * Provide the ability to pass functions as arguments.
 * @param <V> The type of the function which agrees with the
 *            type of the values stored in the tree.
 * @author Dr. Jody Paul
 * @version initial (0.1) [27 August 2013]
 */
interface Function<V> {
    /**
     * Functions are defined by a <code>body</code> method.
     * @param value the argument passed in to the function
     * @return the result of evaluating the function with the given argument
     */
    V body(V value);
}
